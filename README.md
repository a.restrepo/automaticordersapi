# Automatic Orders API

_Application developed in Java to create and deploy a lambda and Api Gateway_

## What it does ...

_Save the body of a request in an Amazon SQS_

### To consider ...

In the body of the request, there must be two mandatory parameters: **authorization** and **stage**. The first parameter must contain the username and password encapsulated in an encrypted key to obtain permission to execute the function. The second parameter contains the value **prod** or **dev** to save the body of the request in the production or development SQS as the case may be. UOnce the SQS to be used has been determined, these two parameters are removed from the request body, the application creates the queue (queueProd.fifo or queueDev.fifo only if necessary) and then saves the request body.

### Example

_From the lambda console:_

```
{
  "body": {
  	"authorization": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  	"stage": "dev",
    ...
  },
  "resource": "/{proxy+}",
  "path": "/path/to/resource",
  "httpMethod": "POST",
  "isBase64Encoded": true,
  "queryStringParameters": {
    "foo": "bar"
  },
  "multiValueQueryStringParameters": {
    "foo": [
      "bar"
    ]
  },
  "pathParameters": {
    "proxy": "/path/to/resource"
  },
  "stageVariables": {
    "baz": "qux"
  },
  ...
 }
```
_From api or a client REST:_

```
{
  "authorization": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "stage": "dev",
  "resource": "/{proxy+}",
  "path": "/path/to/resource",
  "httpMethod": "POST",
  "isBase64Encoded": true,
  "queryStringParameters": {
    "foo": "bar"
  },
  "multiValueQueryStringParameters": {
    "foo": [
      "bar"
    ]
  },
  "pathParameters": {
    "proxy": "/path/to/resource"
  },
  "stageVariables": {
    "baz": "qux"
  },
  ...
 }
```

## Autor

* **Andrés E. Restrepo**

## Thanks to

* A special thanks to **Yeison Cruz** and **Jorge Romero** who helped a lot to make this project possible.

---