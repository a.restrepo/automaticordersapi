package com.atlanticsoft.lambda;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;

import com.atlanticsoft.AutomaticOrdersApi.LambdaFunctionHandler;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class LambdaFunctionHandlerTest {

	private static final String SAMPLE_INPUT_STRING = "{\n"
			+ "  \"authorization\": \"QkVTVEJVWU9SREVSUzo3RnNkJXNsb0ZBODIxJCVWOSlN\",\n" + "  \"text\": \"Test001\"\n" + "  \"messageGroupID\": \"Test001\"\n"
			+ "}";

	// private static final String EXPECTED_OUTPUT_STRING = "{\"FOO\": \"BAR\"}";

	/*
	@Test
	public void testLambdaFunctionHandler() throws IOException {
		LambdaFunctionHandler handler = new LambdaFunctionHandler();
		InputStream input = new ByteArrayInputStream(SAMPLE_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();
		handler.handleRequest(input, output, null);
		// TODO: validate output here if needed.
		String sampleOutputString = output.toString();
		System.out.println(sampleOutputString);
		// Assert.assertEquals(EXPECTED_OUTPUT_STRING, sampleOutputString);
	}
	/**/
}
