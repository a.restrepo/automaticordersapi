package com.atlanticsoft.AutomaticOrdersApi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.atlanticsoft.AutomaticOrdersApi.dto.Request;
import com.atlanticsoft.AutomaticOrdersApi.service.LambdaService;

@SuppressWarnings("unchecked")
public class LambdaFunctionHandler implements RequestStreamHandler {

	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		LambdaService lambdaService = new LambdaService();

		JSONObject event = null;
		JSONParser parser = new JSONParser();
		try {
			// Convert input to JSONObject for extract the data and be mapping to Request
			// class.
			event = (JSONObject) parser.parse(reader);
			JSONObject requestBody = (JSONObject) parser.parse(event.get("body").toString());
			Request request = new Request(requestBody.toString());
			// Request request = new Request(event.toString());
			
			if (lambdaService.authentication(request.getAuthorization())) {
				String stage = (request.getStage() == "" || request.getStage() == null) ? "dev" : request.getStage();
				String messageGroupID = (request.getMessageGroupID() == "" || request.getMessageGroupID() == null) ? "messageGroup1" : request.getMessageGroupID();
				String messageDeduplicationID = (request.getMessageDeduplicationID() == "" || request.getMessageDeduplicationID() == null) ? "false" : request.getMessageDeduplicationID();
				System.out.println(stage+"-"+messageGroupID+"-"+messageDeduplicationID);
				lambdaService.saveInQueue(stage, messageGroupID, messageDeduplicationID, requestBody);
			} else {
				lambdaService.setResponse(401, "", "", "");
			}
		} catch (IOException | ParseException e) {
			lambdaService.setResponse(400, "", "", e.getMessage());
		}
		// Get response and write it as output in the OutputStream
		JSONObject responseJson = lambdaService.getResponse();
		OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
		writer.write(responseJson.toString());
		writer.close();
	}
}