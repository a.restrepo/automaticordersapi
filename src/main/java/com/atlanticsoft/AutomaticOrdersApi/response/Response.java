package com.atlanticsoft.AutomaticOrdersApi.response;

import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class Response {

	public String headers = "custom header value";
	public int statusCode;
	public JSONObject body;

	JSONObject responseBody = new JSONObject();

	public Response() {
	}

	public void setResponse(int statusCode, String cipherText, String sqsMessage, String message) {
		this.statusCode = statusCode;

		switch (statusCode) {
		case 200:
			// this.responseBody.put("Encrypt status", "OK");
			// this.responseBody.put("cipherText", cipherText);
			this.responseBody.put("Save status", sqsMessage);
			break;
		case 201:
			// this.responseBody.put("Encrypt status", "OK");
			// this.responseBody.put("cipherText", cipherText);
			this.responseBody.put("Save status", "OK");
			break;
		case 400:
			this.responseBody.put("Exception", message);
			break;
		case 401:
			this.responseBody.put("Error", "Invalid credentials");
			break;
		case 500:
			// this.responseBody.put("Encrypt status", "Failed");
			this.responseBody.put("Save status", "Failed");
			break;
		}

	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public JSONObject getResponse() {
		JSONObject headerJson = new JSONObject();
		headerJson.put("x-custom-header", this.headers);
		JSONObject responseJson = new JSONObject();
		responseJson.put("headers", headerJson);
		responseJson.put("statusCode", this.statusCode);
		responseJson.put("body", this.responseBody.toString());

		return responseJson;
	}

}
