package com.atlanticsoft.AutomaticOrdersApi.dto;

import com.google.gson.Gson;

@SuppressWarnings("unchecked")
public class Request {
	
	private String authorization;
	private String stage;
	private String messageGroupID;
	private String messageDeduplicationID;
	private String text;
	
	public Request (String json) {
		Gson gson = new Gson();
		Request request = gson.fromJson(json, Request.class);
		this.authorization = request.getAuthorization();
		this.stage = request.getStage();
		this.messageGroupID = request.getMessageGroupID();
		this.messageDeduplicationID = request.getMessageDeduplicationID(); 
		this.text = request.getText();
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getMessageGroupID() {
		return messageGroupID;
	}

	public void setMessageGroupID(String messageGroupID) {
		this.messageGroupID = messageGroupID;
	}
	
	public String getMessageDeduplicationID() {
		return messageDeduplicationID;
	}

	public void setMessageDeduplicationID(String messageDeduplicationID) {
		this.messageDeduplicationID = messageDeduplicationID;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}