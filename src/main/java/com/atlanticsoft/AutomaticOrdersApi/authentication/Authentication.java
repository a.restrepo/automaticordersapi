package com.atlanticsoft.AutomaticOrdersApi.authentication;

import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;

import com.atlanticsoft.AutomaticOrdersApi.dto.User;

public class Authentication {

	User user = new User();

	public boolean authenticate(String authCredentials) {
		// boolean autentication;
		if (authCredentials == null) {
			return false;
		}
		// header value format will be "Basic encodedstring" for Basic authentication
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");

		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();
		
		if (username.equals(user.getUser()) && password.equals(user.getPassword())) {
			return true;
		} else {
			return false;
		}
	}

}