package com.atlanticsoft.AutomaticOrdersApi.criptography;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Criptography {

	private static final String KEY = "TEAADADADA79878979898798";
	private static final String IV = "4657897ADADADAD";

	private static final String ALGORITHM = "Blowfish";
	private static final int LIMIT = 8;
	private static final String CIPHER_TRANSFORMATION = "Blowfish/CBC/PKCS5Padding";

	public String encrypt(String plainText) {
		try {
			byte[] keyBytes = (KEY).getBytes();
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, ALGORITHM);
			byte[] iv = (paddingIV(IV)).getBytes();
			Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
			byte[] cipherText = cipher.doFinal(plainText.getBytes());
			return new String(Base64.getEncoder().encode(cipherText));
		} catch (Exception e) {
			e.printStackTrace();
			return "500";
		}
	}

	public String decrypt(String cipherText) {
		try {
			byte[] keyBytes = (KEY).getBytes();
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, ALGORITHM);
			byte[] iv = (paddingIV(IV)).getBytes();
			Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
			byte[] plainText = cipher.doFinal(java.util.Base64.getDecoder().decode(cipherText));
			return new String(plainText);
		} catch (Exception e) {
			e.printStackTrace();
			return "500";
		}
	}

	private String paddingIV(String iv) {
		iv += new String(new char[32 - iv.length()]).replace("\0", "0");
		iv = iv.substring(0, LIMIT);
		return iv;
	}

}