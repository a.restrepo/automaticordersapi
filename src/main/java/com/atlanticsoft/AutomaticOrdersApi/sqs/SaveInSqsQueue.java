package com.atlanticsoft.AutomaticOrdersApi.sqs;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class SaveInSqsQueue {

	private final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

	public String sqsSendMessageRequest(String queueName, String messageGroupID, String messageDeduplicationID, String data) {

		if (queueName.equals("queueProd.fifo") || queueName.equals("queueDev.fifo")) {
			// Create a FIFO queue
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			final Map<String, String> attributes = new HashMap<String, String>();
			// A FIFO queue must have the FifoQueue attribute set to True
			attributes.put("FifoQueue", "true");
			// If the user doesn't provide a MessageDeduplicationId, generate a
			// MessageDeduplicationId based on the content.
			attributes.put("ContentBasedDeduplication", "true");
			// The FIFO queue name must end with the .fifo suffix
			final CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			final String queueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
			// String queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();

			try {
				// Send a message.
				final SendMessageRequest sendMessageRequest = new SendMessageRequest(queueUrl, data);
				// When you send messages to a FIFO queue, you must provide a non-empty
				// MessageGroupId.
				sendMessageRequest.setMessageGroupId(messageGroupID);
				// Uncomment the following to provide the MessageDeduplicationId
				if(messageDeduplicationID != "false") {
					sendMessageRequest.setMessageDeduplicationId(messageDeduplicationID);
				}
				// final SendMessageResult sendMessageResult =
				sqs.sendMessage(sendMessageRequest);
				return new String("201");
			} catch (final AmazonServiceException ase) {
				Map<String, String> request = new HashMap<>();
				request.put("Error Message: ", ase.getMessage());
				request.put("HTTP Status Code: ", ase.getStatusCode() + "");
				request.put("AWS Error Code:", ase.getErrorCode());
				request.put("Error Type:", ase.getErrorType() + "");
				request.put("Request ID:", ase.getRequestId());
				return "ERROR 404" + request.toString();
			} catch (final AmazonClientException ace) {
				Map<String, String> request = new HashMap<>();
				request.put("Caught an AmazonClientException, which means "
						+ "the client encountered a serious internal problem while "
						+ "trying to communicate with Amazon SQS, such as not "
						+ "being able to access the network. Error Message: ", ace.getMessage());
				return request.toString();
			}
		} else {
			Map<String, String> request = new HashMap<>();
			request.put("Error Message: ", "Queue not found");
			return request.toString();
		}
		/**/
		// return "201";
	}


}