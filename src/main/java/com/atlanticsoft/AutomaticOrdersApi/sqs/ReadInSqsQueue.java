package com.atlanticsoft.AutomaticOrdersApi.sqs;

import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;

public class ReadInSqsQueue {

	private final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

	public void readInSqsQueue() {

		String queueUrl = sqs.getQueueUrl("queueDev.fifo").getQueueUrl();
		// Receive messages
		System.out.println("Receiving messages from queueDev.fifo\n");
		final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
		// Uncomment the following to provide the ReceiveRequestDeduplicationId
		// receiveMessageRequest.setReceiveRequestAttemptId("1");
		final List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
		for (final Message message : messages) {
			System.out.println("Message");
			System.out.println("  MessageId:     " + message.getMessageId());
			System.out.println("  ReceiptHandle: " + message.getReceiptHandle());
			System.out.println("  MD5OfBody:     " + message.getMD5OfBody());
			System.out.println("  Body:          " + message.getBody());
			for (final Entry<String, String> entry : message.getAttributes().entrySet()) {
				System.out.println("Attribute");
				System.out.println("  Name:  " + entry.getKey());
				System.out.println("  Value: " + entry.getValue());
			}
		}
		System.out.println();

	}

}
