package com.atlanticsoft.AutomaticOrdersApi.service;

import org.json.simple.JSONObject;

import com.atlanticsoft.AutomaticOrdersApi.authentication.Authentication;
import com.atlanticsoft.AutomaticOrdersApi.criptography.Criptography;
import com.atlanticsoft.AutomaticOrdersApi.response.Response;
import com.atlanticsoft.AutomaticOrdersApi.sqs.*;

@SuppressWarnings("unchecked")
public class LambdaService {

	Response response = new Response();
	Criptography criptography = new Criptography();
	SaveInSqsQueue sqsQueue = new SaveInSqsQueue();

	// Authentication - Receives authentication string, decrypts it and returns
	// boolean from the operation
	public boolean authentication(String authCredentials) {
		Authentication authentication = new Authentication();
		return (authentication.authenticate(authCredentials));
	}

	// Receives BODY and sent to save
	public void saveInQueue(String stage, String messageGroupID, String messageDeduplicationID, JSONObject data) {
		String queueName = (stage.equals("prod")) ? "queueProd.fifo"
				: (stage.equals("dev")) ? "queueDev.fifo"
						: "noQueue";
		
		data.remove("authorization");
		data.remove("stage");
		String sqsMessage = sqsQueue.sqsSendMessageRequest(queueName, messageGroupID, messageDeduplicationID, data.toString());
		if (sqsMessage.equals("201")) {
			setResponse(201, "cipherText", "OK", "");
		} else {
			setResponse(200, "cipherText", sqsMessage, "");
		}
		/*
		ReadInSqsQueue readInSqsQueue = new ReadInSqsQueue();
		readInSqsQueue.readInSqsQueue();*/
	}

	public void setResponse(int statusCode, String cipherText, String sqsMessage, String message) {
		response.setResponse(statusCode, cipherText, sqsMessage, message);
	}

	public JSONObject getResponse() {
		return response.getResponse();
	}

}